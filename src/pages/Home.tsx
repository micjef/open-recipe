// import MessageListItem from '../components/MessageListItem';
import { useState } from 'react';
// import { Message, getMessages } from '../data/messages';
import { Recipes, getRecipes } from '../assets/data/recipe';
import {
  IonContent,
  IonHeader,
  IonList,
  IonPage,
  IonRefresher,
  IonRefresherContent,
  IonTitle,
  IonToolbar,
  useIonViewWillEnter
} from '@ionic/react';
import './Home.css';
import RecipeCard from '../components/RecipeCard';

const Home: React.FC = () => {

  const [recipes, setRecipes] = useState<Recipes[]>([]);

  useIonViewWillEnter(() => {
    const msgs = getRecipes();
    setRecipes(msgs);
  });

  const refresh = (e: CustomEvent) => {
    setTimeout(() => {
      e.detail.complete();
    }, 3000);
  };

  return (
    <IonPage id="home-page">
      {/* <IonHeader>
        <IonToolbar>
          <IonTitle>Inbox</IonTitle>
        </IonToolbar>
      </IonHeader> */}
      <IonContent fullscreen>
        <IonRefresher slot="fixed" onIonRefresh={refresh}>
          <IonRefresherContent></IonRefresherContent>
        </IonRefresher>

        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">
              Inbox
            </IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonList>
          <div className='recipeHome'>
            {recipes.map(m => <RecipeCard key={m.id} recipe={m} />)}
          </div>
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Home;
