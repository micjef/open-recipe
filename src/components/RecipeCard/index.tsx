import './styles.css'
import { Recipes } from '../../assets/data/recipe';

interface RecipesListItemProps {
  recipe: Recipes;
}

const RecipeCard: React.FC<RecipesListItemProps> = ({ recipe }) => {
  return (
    <div className='recipeCard' style={{ backgroundImage: `url(${recipe.image})`}}>
      <div className="recipeCardBottom">
        <h5 className='recipeCardTitle'>
          {recipe.title.length > 25 ? `${recipe.title.substring(0, 25)}...` : recipe.title}
        </h5>
      </div>
    </div>
  )
}

export default RecipeCard